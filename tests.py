import pprint
import unittest

from main import *


class AesEncryptTests(unittest.TestCase):
	def test_multiply(self):
		state = [
			[0xdb, 0xf2, 0x01, 0xd4],
			[0x13, 0x0a, 0x01, 0xd4],
			[0x53, 0x22, 0x01, 0xd4],
			[0x45, 0x5c, 0x01, 0xd5]
		]

		reference = [
			[0x8e, 0x9f, 0x01, 0xd5],
			[0x4d, 0xdc, 0x01, 0xd5],
			[0xa1, 0x58, 0x01, 0xd7],
			[0xbc, 0x9d, 0x01, 0xd6]
		]

		n_state = mix_columns(state)
		pprint.PrettyPrinter().pprint(n_state)

		for i in range(4):
			for j in range(4):
				self.assertEqual(n_state[i][j], reference[i][j])

	def test_shift_row(self):
		row = [1, 2, 3, 4]
		result = []
		for i in range(4):
			result.append(shift_row(row, i))
		print(result)
		self.assertEqual(result[0][0], 1)
		self.assertEqual(result[0][1], 2)
		self.assertEqual(result[0][2], 3)
		self.assertEqual(result[0][3], 4)

	def test_key_expansion(self):
		key = bytes(
			[0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c])
		schedule = key_expansion(key)
		print(schedule)

		self.assertEqual(len(schedule), 44)
		self.assertEqual(schedule[4], bytes([0xa0, 0xfa, 0xfe, 0x17]))
		self.assertEqual(schedule[21], bytes([0x7c, 0x83, 0x9d, 0x87]))
		self.assertEqual(schedule[27], bytes([0xca, 0x00, 0x93, 0xfd]))
		self.assertEqual(schedule[43], bytes([0xb6, 0x63, 0x0c, 0xa6]))

	def test_encrypt_block(self):
		block = bytes(
			[0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34])
		key = bytes(
			[0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c])
		output = encrypt_block(block, key)
		ref_output = [
			[0x39, 0x02, 0xdc, 0x19],
			[0x25, 0xdc, 0x11, 0x6a],
			[0x84, 0x09, 0x85, 0x0b],
			[0x1d, 0xfb, 0x97, 0x32]
		]

		for i in range(4):
			for j in range(4):
				self.assertEqual(output[i][j], ref_output[i][j])


class AesDecryptTests(unittest.TestCase):
	def test_shift_rows(self):
		rows = [[1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4]]
		result = inv_shift_rows(rows)
		print(result)
		self.assertEqual(result[0][0], 1)
		self.assertEqual(result[1][0], 4)
		self.assertEqual(result[2][0], 3)
		self.assertEqual(result[3][0], 2)


	def test_decrypt_block(self):
		original_text = bytes.fromhex("00 11 22 33 44 55 66 77 88 99 aa bb cc dd ee ff")
		key = bytes.fromhex("00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f")
		ref_ciphertext = bytes.fromhex("69 c4 e0 d8 6a 7b 04 30 d8 cd b7 80 70 b4 c5 5a")
		output = decrypt_block(ref_ciphertext, key)
		output = destatify_(output)
		self.assertEqual(original_text, output)


if __name__ == '__main__':
	unittest.main()
